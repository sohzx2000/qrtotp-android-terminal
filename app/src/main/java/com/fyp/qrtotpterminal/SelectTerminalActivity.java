package com.fyp.qrtotpterminal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SelectTerminalActivity extends AppCompatActivity {
    private static final String TAG = "SelectTerminalActivity";

    /* Views in Layout */
    private TextView tvSelected;
    private RecyclerView rvTerminals;
    private Button btnConfirm;

    private TerminalRecyclerAdapter terminalRecyclerAdapter;
    private ArrayList<String> terminalList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_terminal);

        // get data from GenerateActivity (if calling activity)
        boolean skip = getIntent().getBooleanExtra("skip", true);

        // Check if terminalID present in shared preferences
        if (skip && Utility.getPreference(SelectTerminalActivity.this, Utility.SP_TERMINAL_ID) != null) {
            finishAffinity();
            startActivity(new Intent(SelectTerminalActivity.this, GenerateActivity.class));
        }

        // Find all views
        tvSelected = findViewById(R.id.textView_selected);
        rvTerminals = findViewById(R.id.recyclerView_terminals);
        btnConfirm = findViewById(R.id.button_confirm);

        // Setup views
        rvTerminals.setLayoutManager(new LinearLayoutManager(this));
        terminalRecyclerAdapter = new TerminalRecyclerAdapter(this, terminalList, terminalSelectListener);
        rvTerminals.setAdapter(terminalRecyclerAdapter);

        // Get terminals from server
        RailsRequester.AsyncNormal asyncGetTerminals = new RailsRequester.AsyncNormal(SelectTerminalActivity.this, getTerminalListener, RailsRequester.REQUEST_TYPE.GET_TERMINALS, null);
        asyncGetTerminals.execute();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvSelected.getText().toString().isEmpty()) {
                    // proceed to staff login
                    Intent intent = new Intent(SelectTerminalActivity.this, ConfirmSetupActivity.class);
                    intent.putExtra("terminal_name", tvSelected.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(SelectTerminalActivity.this, "Please select a terminal", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    /** --------------------------------------------- CLICK LISTENERS --------------------------------------------- **/


    private View.OnClickListener terminalSelectListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            tvSelected.setText(terminalList.get(position));
            tvSelected.setVisibility(View.VISIBLE);
            Log.d(TAG, "onClick: link position " + position);
        }
    };


    /** --------------------------------------------- RAILS LISTENERS --------------------------------------------- **/


    private RailsRequester.AsyncNormal.AsyncListener getTerminalListener = new RailsRequester.AsyncNormal.AsyncListener() {
        @Override
        public void OnRequestComplete(String result) {
            JSONResultHelper jsonResultHelper = new JSONResultHelper(SelectTerminalActivity.this, result);
            if (jsonResultHelper.resultSuccess()) {
                Log.d(TAG, "getKey: OnRequestComplete: success");
                terminalList = jsonResultHelper.getTerminals("terminals");
                terminalRecyclerAdapter.loadNewData(terminalList);
            } else {
                int errorCode = jsonResultHelper.getErrorCode();
                Toast.makeText(SelectTerminalActivity.this, "Error getting terminals: " + ErrorCode.getErrorMessage(errorCode), Toast.LENGTH_LONG).show();
            }
        }
    };
}
