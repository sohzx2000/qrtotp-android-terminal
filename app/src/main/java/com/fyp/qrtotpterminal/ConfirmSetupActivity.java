package com.fyp.qrtotpterminal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConfirmSetupActivity extends AppCompatActivity {
    private static final String TAG = "ConfirmSetupActivity";

    /* Views in Layout */
    private EditText etUsername;
    private EditText etPassword;
    private Button btnSignin;

    private String terminalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_setup);

        // get intent extra
        terminalName = getIntent().getStringExtra("terminal_name");

        // get views
        etUsername = findViewById(R.id.editText_username);
        etPassword = findViewById(R.id.editText_password);
        btnSignin = findViewById(R.id.button_signin);

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RailsRequester.AsyncNormal asyncConfirmSetup = new RailsRequester.AsyncNormal(ConfirmSetupActivity.this, confirmSetupListener, RailsRequester.REQUEST_TYPE.CONFIRM_SETUP, null);
                asyncConfirmSetup.execute(etUsername.getText().toString(), etPassword.getText().toString(), terminalName);
            }
        });
    }


    /** --------------------------------------------- RAILS LISTENERS --------------------------------------------- **/


    private RailsRequester.AsyncNormal.AsyncListener confirmSetupListener = new RailsRequester.AsyncNormal.AsyncListener() {
        @Override
        public void OnRequestComplete(String result) {
            JSONResultHelper jsonResultHelper = new JSONResultHelper(ConfirmSetupActivity.this, result);
            if (jsonResultHelper.resultSuccess()) {
                Log.d(TAG, "getKey: OnRequestComplete: success");
                // save terminal ID
                int terminalID = jsonResultHelper.getKeyInt("terminal_id");
                Utility.setPreference(ConfirmSetupActivity.this, Utility.SP_TERMINAL_ID, String.valueOf(terminalID));
                Utility.setPreference(ConfirmSetupActivity.this, Utility.SP_TERMINAL_NAME, String.valueOf(terminalName));
                finishAffinity();
                startActivity(new Intent(ConfirmSetupActivity.this, GenerateActivity.class));
            } else {
                int errorCode = jsonResultHelper.getErrorCode();
                Toast.makeText(ConfirmSetupActivity.this, "Error confirming changes: " + ErrorCode.getErrorMessage(errorCode), Toast.LENGTH_LONG).show();
            }
        }
    };
}
