package com.fyp.qrtotpterminal;

public class ErrorCode {
    // Internal Error Codes
    public static final int CONNECTION_ERROR = 901;
    public static final int JSON_ERROR = 902;

    // Server Error Codes
    public static final int BAD_TOKEN = 10;
    public static final int TERMINAL_NOT_FOUND_ERROR = 13;
    public static final int STAFF_NOT_FOUND_ERROR = 14;

    /**
     * Returns a String error message corressponding to the given errorCode
     *
     * @param errorCode     The errorCode
     * @return  refer to method description
     */
    public static String getErrorMessage(int errorCode) {
        switch (errorCode) {
            case CONNECTION_ERROR: return "Connection error";
            case JSON_ERROR: return "Invalid JSON returned from server";
            case BAD_TOKEN: return "Bad token";
            case TERMINAL_NOT_FOUND_ERROR: return "Terminal ID does not exist";
            case STAFF_NOT_FOUND_ERROR: return "Invalid username or password";
            default: return "Error";
        }
    }
}
