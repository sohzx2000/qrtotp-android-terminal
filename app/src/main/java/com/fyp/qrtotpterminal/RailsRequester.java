package com.fyp.qrtotpterminal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import static com.fyp.qrtotpterminal.ErrorCode.JSON_ERROR;

/**
 * Created by UserPC on 4/5/2018.
 */

public class RailsRequester {
    private static final String TAG = "RailsRequester";

    enum REQUEST_TYPE {
        CONFIRM_SETUP("terminal/confirm_setup", true, true),
        GET_SERVER_TIME("terminal/get_server_time", true, true),
        GET_KEY("terminal/get_key", true, true),
        SET_DISCREPANCY("terminal/set_discrepancy", true, true),
        GET_TERMINALS("terminal/get_terminals", false, false);

        private final String address;
        private final boolean byPost;
        private final boolean requireToken;

        REQUEST_TYPE(String address, boolean byPost, boolean requireToken) {
            this.address = address;
            this.byPost = byPost;
            this.requireToken = requireToken;
        }

        public String getAddress() {
            return address;
        }

        public boolean isByPost() {
            return byPost;
        }

        public boolean isRequireToken() {
            return requireToken;
        }
    }

    static class AsyncNormal extends AsyncTask<String, Void, String> {

        public interface AsyncListener {
            void OnRequestComplete(String result);
        }

        private WeakReference<Context> activityReference;
        private AsyncListener listener;
        private REQUEST_TYPE requestType;
        private JSONObject prebuiltJObject;

        AsyncNormal(Context context, AsyncListener listener, REQUEST_TYPE requestType, JSONObject prebuiltJObject) {
            activityReference = new WeakReference<>(context);
            this.listener = listener;
            this.requestType = requestType;
            this.prebuiltJObject = prebuiltJObject;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jObject;

            try {
                jObject = new JSONObject();
                switch (requestType) {
                    case CONFIRM_SETUP:
                        jObject.put("username", params[0]);
                        jObject.put("password", params[1]);
                        jObject.put("terminal_name", params[2]);
                        break;
                    case GET_SERVER_TIME:
                        jObject.put("device_request_time", params[0]);
                        break;
                    case GET_KEY:
                        jObject.put("terminal_id", params[0]);
                        break;
                    case SET_DISCREPANCY:
                        jObject.put("terminal_id", params[0]);
                        jObject.put("time_discrepancy", Integer.parseInt(params[1]));
                        break;
                    case GET_TERMINALS:
                        jObject = null;
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return ErrorCode.getErrorMessage(JSON_ERROR);
            }


            if (isCancelled()) {
                return null;
            } else {
                String requestMethod = "GET";
                String authToken = null;
                if (requestType.isByPost()) requestMethod = "POST";
                if (requestType.isRequireToken()) authToken = Utility.getAuthToken(activityReference.get());
                return Utility.getServerResponse(requestType.getAddress(), requestMethod, authToken, jObject);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute: " + result);
            listener.OnRequestComplete(result);
        }
    }
}

