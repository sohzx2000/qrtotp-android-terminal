package com.fyp.qrtotpterminal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TerminalRecyclerAdapter extends RecyclerView.Adapter<TerminalRecyclerAdapter.TerminalViewHolder>  {
    private static final String TAG = "TerminalRecyclerAdapter";
    
    private List<String> terminalList;
    private Context context;
    private View.OnClickListener listener;

    public TerminalRecyclerAdapter(Context context, List<String> terminals, View.OnClickListener listener) {
        this.context = context;
        this.terminalList = terminals;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TerminalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.terminal_list_item, parent, false);
        return new TerminalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TerminalViewHolder holder, int position) {
        // Called by the layout manager when it wants new data in an existing row
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(listener);
        String terminalItem = terminalList.get(position);
        holder.tvTerminalName.setText(terminalItem);
    }

    @Override
    public int getItemCount() {
        return ((terminalList != null) && (terminalList.size() != 0) ? terminalList.size() : 0);
    }

    /**
     * Replaces current terminal list with given terminal list
     *
     * @param terminalList      New list of terminals
     */
    void loadNewData(List<String> terminalList) {
        if (this.terminalList != null) {
            this.terminalList.clear();
        }
        this.terminalList.addAll(terminalList);
        notifyDataSetChanged();
    }

    public List<String> getTerminalList() {
        return terminalList;
    }

    static class TerminalViewHolder extends RecyclerView.ViewHolder {
        TextView tvTerminalName;

        public TerminalViewHolder(View view) {
            super(view);
            this.tvTerminalName = view.findViewById(R.id.textView_terminalName);
        }
    }
}
