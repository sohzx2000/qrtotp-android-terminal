package com.fyp.qrtotpterminal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class JSONResultHelper {
    private static final String TAG = "JSONResultHelper";
    private JSONObject jObject = null;
    private boolean success;
    private int errorCode;
    private Context context;

    JSONResultHelper(Context context, String jString) {
        this.context = context;
        parseRawResult(jString);
    }

    /**
     * Parses the JSON result retrieved from the server, checking if the request was successful.
     * If not, retrieve the error code from the result.
     * Also catches the BAD_TOKEN error code, clearing the SharedPreferences
     *
     * @param jString   the JSON response returned from the server
     */
    private void parseRawResult(String jString) {

        String jsonResult = "";

        if (jString.equalsIgnoreCase(ErrorCode.getErrorMessage(ErrorCode.JSON_ERROR))) {
            success = false;
            errorCode = ErrorCode.JSON_ERROR;
        } else if (jString.equalsIgnoreCase(ErrorCode.getErrorMessage(ErrorCode.CONNECTION_ERROR))) {
            success = false;
            errorCode = ErrorCode.CONNECTION_ERROR;
        } else {
            try {
                jObject = new JSONObject(jString);
                jsonResult = jObject.getString("result");

                if (jsonResult.equalsIgnoreCase("success")) {
                    success = true;

                }
                if (jsonResult.equalsIgnoreCase("failure")) {
                    success = false;
                    errorCode = jObject.getInt("error");
                }
            } catch (JSONException e) {
                success = false;
                errorCode = ErrorCode.CONNECTION_ERROR;
                e.printStackTrace();
            }
        }

        // Catch BAD_TOKEN here
        if (errorCode == ErrorCode.BAD_TOKEN) {
            // clear preferences and go back to signin page
            Utility.clearSharedPreferences(context);
            Intent intent = new Intent(context, SelectTerminalActivity.class);
            context.startActivity(intent);
            ((Activity)context).finishAffinity();
        }
    }

    public boolean resultSuccess() {
        return success;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getKeyString(String key) {
        try {
            return jObject.getString(key);
        } catch(JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer getKeyInt(String key) {
        try {
            return jObject.getInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Boolean getKeyBoolean(String key) {
        try {
            return jObject.getBoolean(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns an arraylist of terminal names
     * @param key   the JSON key to retreive the terminal array
     * @return  refer method description
     */
    public ArrayList<String> getTerminals(String key) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            JSONArray jArray = jObject.getJSONArray(key);
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                arrayList.add(i, jObject.getString("name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        Log.d(TAG, "getTerminals: " + arrayList.get(0));
        return arrayList;
    }

    /**
     * Checks if a JSON key is available in a given JSONObject
     *
     * @param jsonObject    the JSONObject to check in
     * @param key           the key to be looking for
     * @return  refer method description
     */
    private boolean keyAvailable(JSONObject jsonObject, String key) {
        return !jsonObject.isNull(key);
    }
}
