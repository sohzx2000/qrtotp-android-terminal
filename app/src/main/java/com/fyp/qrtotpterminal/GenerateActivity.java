package com.fyp.qrtotpterminal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class GenerateActivity extends AppCompatActivity {
    private static final String TAG = "GenerateActivity";

    /* Views in Layout */
    private ImageView imageView;
    private Button btnSetup;

    private final int QR_SIZE = 1000;
    private final int TIME_GAP = 1000;
    private final int CYCLE_DURATION = (10 * TIME_GAP) + 100; // add 100 to account for time drifts
    private int terminalID;

    private Calendar calendar;
    private long timeMillis;
    private CountDownTimer countDownTimer;

    private String key;

    private int count = 0;
    private final int PING_COUNT = 21;  // TODO: 4/17/2019 figure out why first ping always takes longer
    private TimerTask calculateLatencyTimerTask;
    private Timer calculateLatencyTimer = new Timer(true);
    private ArrayList<Long> toLatencies = new ArrayList<>();
    private ArrayList<Long> roundTripTimes = new ArrayList<>();
    private long averageToLatency;
    private long averageRTT;
    private long timeDiscrepancy;
    private boolean timeDiscrepancyDone= false;

    private Timer timer;
    private GeneratorTask generatorTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate);

        // get views
        imageView = findViewById(R.id.imageView_qr);
        btnSetup = findViewById(R.id.button_setup);

        // get temrinal ID and name
        terminalID = Integer.parseInt(Utility.getPreference(GenerateActivity.this, Utility.SP_TERMINAL_ID));
        String terminalName = Utility.getPreference(GenerateActivity.this, Utility.SP_TERMINAL_NAME);

        // get key from server
        RailsRequester.AsyncNormal asyncGetKey = new RailsRequester.AsyncNormal(GenerateActivity.this, getKeyListener, RailsRequester.REQUEST_TYPE.GET_KEY, null);
        asyncGetKey.execute(String.valueOf(terminalID));

        btnSetup.setText(terminalName);
        btnSetup.setVisibility(View.VISIBLE);
        btnSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculateLatencyTimer.cancel();
                calculateLatencyTimerTask.cancel();
                countDownTimer.cancel();
                Intent intent = new Intent(GenerateActivity.this, SelectTerminalActivity.class);
                intent.putExtra("skip", false);
                startActivityForResult(intent, 0);
            }
        });

        // calculate latency
        calculateLatencyTimerTask = new TimerTask() {
            public void run() {
                if (count == PING_COUNT) {
                    calculateLatencyTimer.cancel();
                } else {
                    calculateLatency();
                }
                count++;
            }
        };
        calculateLatencyTimer.scheduleAtFixedRate(calculateLatencyTimerTask, 0, 1 * 1000);

        countDownTimer = new CountDownTimer(CYCLE_DURATION, TIME_GAP) {
            public void onTick(long millisUntilFinished) {
                // get system time rounded-down to TIME_GAP
                calendar = Calendar.getInstance();
                timeMillis = calendar.getTime().getTime();
                long roundedTimeMillis = timeMillis - (timeMillis % TIME_GAP);

                // hash roundedTimeMillis with salt (SHA-256, hexadecimal)
                String hash = null;
                try {
                   MessageDigest md = MessageDigest.getInstance("SHA-256");
                   String concatenated = String.valueOf(key + roundedTimeMillis);
                   byte[] digest = md.digest(concatenated.getBytes());
                   hash = bytesToHex(digest).substring(0, 16);
                } catch (NoSuchAlgorithmException e) {
                   e.printStackTrace();
                }
                Log.d(TAG, "onTick: " + hash);

                // display QR code
                if (hash != null)
                    imageView.setImageBitmap(generateQR(getApplicationContext(), "QRTOTP:" + terminalID + ":" + hash, QR_SIZE));
                Log.d(TAG, "onTick: " + timeMillis);
                Log.d(TAG, "onTick: " + roundedTimeMillis);
            }

            public void onFinish() {
//                this.start();
                startGenerationCycle(); // restarts the cycle
                // TODO: 4/16/2019 possible resync?
            }
        };
    }

    /**
     * 'Pings' server to calculate latency
     */
    private void calculateLatency() {
        RailsRequester.AsyncNormal asyncGetServerTime = new RailsRequester.AsyncNormal(GenerateActivity.this, getServerTimeListener, RailsRequester.REQUEST_TYPE.GET_SERVER_TIME, null);
        long deviceRequestTime = Calendar.getInstance().getTime().getTime();
        asyncGetServerTime.execute(String.valueOf(deviceRequestTime));
    }

    /**
     * Calculates time discrepancy from server based on toLatency and RTT/2
     * result is sent to server before QR generation starts
     */
    private void calculateTimeDiscrepancy() {
        long totalToLatency = 0;
        for (int i = 1; i < toLatencies.size(); i++) {  // ignore first record as it is always significantly bigger
            long toLatency = toLatencies.get(i);
            totalToLatency += toLatency;
        }
        averageToLatency = totalToLatency/(toLatencies.size()-1);

        long totalRTT = 0;
        for (int i = 1; i < roundTripTimes.size(); i++) {   // ignore first record as it is always significantly bigger
            long roundTripTime = roundTripTimes.get(i);
            totalRTT += roundTripTime;
        }
        averageRTT = totalRTT/(roundTripTimes.size()-1);

        timeDiscrepancy = averageToLatency - (averageRTT / 2);
        timeDiscrepancyDone = true;
        Log.d(TAG, "Latency: " + averageToLatency + " RTT:" + averageRTT + " Discrepancy: " + timeDiscrepancy);

        // send time discrepancy to server
        RailsRequester.AsyncNormal asyncSetDiscrepancy = new RailsRequester.AsyncNormal(GenerateActivity.this, setDiscrepancyListener, RailsRequester.REQUEST_TYPE.SET_DISCREPANCY, null);
        asyncSetDiscrepancy.execute(String.valueOf(terminalID), String.valueOf(timeDiscrepancy));

//        countDownTimer.start();
        startGenerationCycle();
    }

    /**
     * Starts QR generation at a multiple of TIME_GAP
     */
    private void startGenerationCycle() {
        Log.d(TAG, "startGenerationCycle: " + "New Cycle");
        if (timer != null) {
            timer.cancel();
            generatorTask.cancel();
        }
        timer = new Timer();
        generatorTask = new GeneratorTask();
        long startTime = Calendar.getInstance().getTime().getTime() + TIME_GAP;
        long roundedStartTime = startTime - (startTime % TIME_GAP);
        timer.schedule(generatorTask, new Date(roundedStartTime));
    }

    /**
     * Converts byte array into hexadecimal string
     *
     * @param bytes     byte array to be converted
     * @return          hexadecimal representation of @bytes
     */
    private String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }


    /**
     * Generates a QR Bitmap containing given text data
     *
     * @param context   context, required by ContextCompat.getColor()
     * @param text      the text to be converted into a QR code
     * @return  refer method Description
     */
    private Bitmap generateQR(Context context, String text, int size) {
        Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
        hints.put(EncodeHintType.MARGIN, 2);

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, size, size, hints);

            // Change colors
            int dataColor = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorPrimaryDark)));
            int backgroundColor = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorPrimaryLight)));

            final int width = bitMatrix.getWidth();
            final int height = bitMatrix.getHeight();
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = bitMatrix.get(x, y) ? dataColor : backgroundColor;
                }
            }

            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (timeDiscrepancyDone) {
            startGenerationCycle();
        } else {
            Log.d(TAG, "onActivityResult: " + count);
            calculateLatencyTimerTask = new TimerTask() {
                public void run() {
                    if (count == PING_COUNT) {
                        calculateLatencyTimer.cancel();
                    } else {
                        calculateLatency();
                    }
                    count++;
                }
            };
            calculateLatencyTimer = new Timer(true);
            calculateLatencyTimer.scheduleAtFixedRate(calculateLatencyTimerTask, 0, 1 * 1000);
        }
    }

    /** --------------------------------------------- CLASSES --------------------------------------------- **/

    private class GeneratorTask extends TimerTask {
        @Override
        public void run() {
            countDownTimer.start();
        }
    };


    /** --------------------------------------------- RAILS LISTENERS --------------------------------------------- **/


    private RailsRequester.AsyncNormal.AsyncListener getServerTimeListener = new RailsRequester.AsyncNormal.AsyncListener() {
        @Override
        public void OnRequestComplete(String result) {
            long deviceReceiveTime = Calendar.getInstance().getTime().getTime();
            JSONResultHelper jsonResultHelper = new JSONResultHelper(GenerateActivity.this, result);
            if (jsonResultHelper.resultSuccess()) {
                Log.d(TAG, "getServerTime: OnRequestComplete: success");

                // get 'to' latency
                long toLatency = jsonResultHelper.getKeyInt("to_latency");
                toLatencies.add(toLatency);
                Log.d(TAG, "OnRequestComplete: " + "toLatency: " + toLatency);

                // calculate RTT
                long deviceRequestTime = Long.parseLong(jsonResultHelper.getKeyString("request_time"));
                long roundTripTime = deviceReceiveTime - deviceRequestTime;
                roundTripTimes.add(roundTripTime);
                Log.d(TAG, "OnRequestComplete: " + "RTT: " + roundTripTime);

                if (toLatencies.size() == PING_COUNT && roundTripTimes.size() == PING_COUNT) {
                    calculateTimeDiscrepancy();
                }
            } else {
                int errorCode = jsonResultHelper.getErrorCode();
                Toast.makeText(GenerateActivity.this, "Error getting server time: " + ErrorCode.getErrorMessage(errorCode), Toast.LENGTH_LONG).show();
            }
        }
    };

    private RailsRequester.AsyncNormal.AsyncListener getKeyListener = new RailsRequester.AsyncNormal.AsyncListener() {
        @Override
        public void OnRequestComplete(String result) {
            JSONResultHelper jsonResultHelper = new JSONResultHelper(GenerateActivity.this, result);
            if (jsonResultHelper.resultSuccess()) {
                Log.d(TAG, "getKey: OnRequestComplete: success");
                key = jsonResultHelper.getKeyString("key");
                Utility.setPreference(GenerateActivity.this, Utility.SP_TERMINAL_KEY, key);
            } else {
                int errorCode = jsonResultHelper.getErrorCode();
                Toast.makeText(GenerateActivity.this, "Error getting terminal key: " + ErrorCode.getErrorMessage(errorCode), Toast.LENGTH_LONG).show();
            }
        }
    };

    private  RailsRequester.AsyncNormal.AsyncListener setDiscrepancyListener = new RailsRequester.AsyncNormal.AsyncListener() {
        @Override
        public void OnRequestComplete(String result) {
            JSONResultHelper jsonResultHelper = new JSONResultHelper(GenerateActivity.this, result);
            if (jsonResultHelper.resultSuccess()) {
                Log.d(TAG, "setDiscrepancy: OnRequestComplete: success");
            } else {
                int errorCode = jsonResultHelper.getErrorCode();
                Toast.makeText(GenerateActivity.this, "Error setting time discrepancy: " + ErrorCode.getErrorMessage(errorCode), Toast.LENGTH_LONG).show();
            }
        }
    };
}
