package com.fyp.qrtotpterminal;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.fyp.qrtotpterminal.ErrorCode.CONNECTION_ERROR;

public class Utility {
    public static final String SP_AUTH_TOKEN = "auth_token";
    public static final String SP_TERMINAL_KEY = "terminal_key";
    public static final String SP_TERMINAL_ID = "terminal_id";
    public static final String SP_TERMINAL_NAME = "terminal_name";

    private static final String TAG = "Utility";
    private static final String HOST_ADDRESS = "http://QRTOTP-dev.ap-southeast-1.elasticbeanstalk.com/";
//        private static final String HOST_ADDRESS = "http://10.0.2.2:3000/";
    public static final int CONNECTION_TIMEOUT = 20000;
    public static final int READ_TIMEOUT = 25000;

    /**
     * Sends a JSON string to the Rails server and returns the server's response
     *
     * @param address           The address (not including root URL) to send the request to
     * @param requestMethod     The request method, POST or GET
     * @param authorization     The auth_token
     * @param jObject           The JSONObject that will be sent as a string
     * @return  The server's response
     */
    public final static String getServerResponse(String address, String requestMethod, String authorization, JSONObject jObject) {
        HttpURLConnection conn = null;
        OutputStream os = null;
        InputStream is = null;
        URL url = null;

        try {   // ensure all connection resources are closed with finally clause
            try {
                // Enter URL address where your php file resides
                url = new URL(HOST_ADDRESS + address);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return ErrorCode.getErrorMessage(CONNECTION_ERROR);
            }

            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod(requestMethod);
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestProperty("Content-Type","application/json");
                conn.setRequestProperty("Accept", "application/json");
                if (authorization != null) {
                    conn.setRequestProperty("Authorization", authorization);
                }

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);

                if (jObject != null) {
                    Log.d(TAG, "getServerResponse: JSON sent " + jObject.toString());
                    // Open connection for sending data
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8")
                    );
                    writer.write(jObject.toString());
                    writer.flush();
                    writer.close();
                    os.close();
                }

                conn.connect();

            } catch (IOException e1) {
                e1.printStackTrace();
                conn.disconnect();
                return ErrorCode.getErrorMessage(CONNECTION_ERROR);
            }

            try {
                int response_code = conn.getResponseCode();
                Log.d("getServerResponse", "Response Code " + Integer.toString(response_code));

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    is = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    is.close();

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {
                    return ErrorCode.getErrorMessage(CONNECTION_ERROR);
                }

            } catch (IOException e) {
                e.printStackTrace();
                return ErrorCode.getErrorMessage(CONNECTION_ERROR);
            } finally {
                conn.disconnect();
            }
        } finally {
            if (conn != null) { conn.disconnect(); }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Sets a string shared preference key value pair
     *
     * @param context   context, required by PreferenceManager
     * @param key       the key to set
     * @param value     the value the key should hold
     */
    public final static void setPreference(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Retrieves the value of a given key in shared preferences
     *
     * @param context   context, required by PreferenceManager
     * @param key       the key to get the value from
     * @return  refer method description
     */
    public final static String getPreference(Context context, String key) {
        String value;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.contains(key)) {
            value = preferences.getString(key, null);
            return value;
        } else {
            return null;
        }
    }

    /**
     * Gets the user's auth_token from shared preferences
     *
     * @param context   context, required by PreferenceManager
     * @return  refer method description
     */
    public static String getAuthToken(Context context) {
        // Retrieve auth_token from shared preferences
        String auth_token = Utility.getPreference(context, Utility.SP_AUTH_TOKEN);
        return "Token token=" + auth_token;
    }

    /**
     * Clears shared preferences containing user specific info
     *
     * @param context   context, required by PreferenceManager
     */
    public static void clearSharedPreferences(Context context) {
        Utility.setPreference(context, Utility.SP_AUTH_TOKEN, null);
        Utility.setPreference(context, Utility.SP_TERMINAL_ID, null);
        Utility.setPreference(context, Utility.SP_TERMINAL_KEY, null);
        Utility.setPreference(context, Utility.SP_TERMINAL_NAME, null);
    }
}
